﻿using System;
using System.Linq;

namespace Teste
{
    class Program
    {
        static void Main(string[] args)
        {
            int MAX_INT = CalcularMaxInt();
            int[][] lists = GerarArrayDeArrays(MAX_INT);

            for (int i = 0; i < MAX_INT; i++) {
                var somaArray = lists[i].Sum();
                for (int j = 0; j < lists[i].Count(); j++) {
                    var tamanhoTijolo = lists[i][j];
                }
            }

            Console.ReadKey();
        }

        private static int CalcularMaxInt() {
            return new Random().Next(1, 10000);
        }

        private static int[][] GerarArrayDeArrays(int maxInt) {
            var array = new int[][] {};
            for (int i = 0; i < maxInt; i++) {
                array.Append(GerarArray(maxInt));
            }
            return array;
        }

        private static int[] GerarArray(int maxInt) {
            var array = new int[] {};
            for (int i = 0; i < maxInt; i++) {
                array.Append(CalcularValorIncluirNoArray(array, maxInt));
            }

            return array;
        }

        private static int CalcularValorIncluirNoArray(int[] array, int maxInt) {
            var finalizado = false;
            var soma = array.Sum();
            var valor = 0;

            while (finalizado) {
                valor = new Random().Next(1, maxInt);
                soma += valor;
                if (soma <= maxInt) {
                    finalizado = true;
                }
            }

            return valor;
        }
    }
}
